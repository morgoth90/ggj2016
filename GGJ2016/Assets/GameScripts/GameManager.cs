﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GamepadInput;

public class GameManager : MonoBehaviour
{
    private static GameManager instance;

    public static GameManager Instance
    {
        get
        {
            return instance;
        }
    }

    public Transform guiCanvas;

    public Transform[] spawnPoints;
    public Transform[] critterSpawnPoints;

    [HideInInspector]
    public PlayerController[] players;

    private GameState gameState;
    private List<CritterController> critters;

    [HideInInspector]
    public List<IngredientType> ingredientSequence = new List<IngredientType>();

    private List<IngredientType> allowedIngredients = new List<IngredientType>();

    void Awake()
    {
        instance = this;

        critters = new List<CritterController>();
    }

    void OnEnable()
    {
        GameEventManager.OnGameStateChanged += OnGameStateChanged;
        GameEventManager.OnCritterDied += OnCritterDied;
        GameEventManager.OnTeamWin += OnTeamWin;
    }

    void OnDisable()
    {
        GameEventManager.OnGameStateChanged -= OnGameStateChanged;
        GameEventManager.OnCritterDied -= OnCritterDied;
        GameEventManager.OnTeamWin -= OnTeamWin;
    }

    void Start()
    {
        AssignAllowedIngredients();
        foreach (IngredientType t in allowedIngredients)
        {
            Debug.Log("Allowed ingredient: " + t);
        }

        LoadPlayers();
        LoadCauldrons();

        StartCoroutine(SpawnCritterCoroutine());


    }

    public void StartGame()
    {
        foreach (PlayerController player in players)
            player.active = true;
    }

    public void SwapTeamControls(TeamType team)
    {
        foreach (PlayerController player in players)
            if (player.teamType == team)
                player.playerNumber = (player.playerNumber) % 2 + (int)player.teamType * 2 + 1;
    }

    private void OnGameStateChanged(GameState gameState)
    {
        this.gameState = gameState;

        switch (gameState)
        {
            case GameState.Play:
                StartCoroutine(SpawnCritterCoroutine());
                break;

            case GameState.GameOver:
                break;
        }
    }

    private void OnCritterDied(CritterController critter)
    {
        for (int i = 0; i < critters.Count; i++)
        {
            if (critters[i] == critter)
            {
                critters.Remove(critter);

                ParticleFactory.CreateParticle(ParticleType.CritterDeath, critter.transform.position);

                Destroy(critter.gameObject);

                break;
            }
        }
    }

    public AnimationCurve winguiCurve;
    public Sprite winguiRedText, winguiRedBg, winguiRedMonster;

    private void OnTeamWin(TeamType teamType)
    {
        foreach (PlayerController player in players)
            player.active = false;

        GameEventManager.TriggerGameStateChanged(GameState.GameOver);

        GameObject blackBg = Instantiate((GameObject)Resources.Load("blackBg"));
        blackBg.transform.SetParent(guiCanvas, false);

        GameObject gui = Instantiate((GameObject)Resources.Load("WinGui"));
        gui.transform.SetParent(guiCanvas, false);
        gui.transform.localPosition = -Vector3.right * 1200f;

        StartCoroutine(OpenRetryGui());

        LTDescr d = LeanTween.moveLocal(gui, Vector3.zero, 5f);
        d.animationCurve = winguiCurve;

        if (teamType == TeamType.TeamTwo)
        {
            gui.transform.FindChild("TEXT").GetComponent<UnityEngine.UI.Image>().sprite = winguiRedText;
            gui.transform.FindChild("BG").GetComponent<UnityEngine.UI.Image>().sprite = winguiRedBg;
            gui.transform.FindChild("MONSTER").GetComponent<UnityEngine.UI.Image>().sprite = winguiRedMonster;
        }
    }

    IEnumerator OpenRetryGui()
    {
        yield return new WaitForSeconds(5);
        while (true)
        {
            yield return null;
            if (GamePad.GetButtonDown(GamePad.Button.A, GamePad.Index.Any) ||
            GamePad.GetButtonDown(GamePad.Button.B, GamePad.Index.Any) ||
            GamePad.GetButtonDown(GamePad.Button.X, GamePad.Index.Any) ||
            GamePad.GetButtonDown(GamePad.Button.Y, GamePad.Index.Any))
            {
                RetryGui.Create();
                yield break;
            }
        }
    }

    private void LoadPlayers()
    {
        players = new PlayerController[4];
        players[0] = PlayerController.Create(1, spawnPoints[0].position, TeamType.TeamOne, PlayerType.Collector);
        players[1] = PlayerController.Create(2, spawnPoints[1].position, TeamType.TeamOne, PlayerType.Slasher);
        players[2] = PlayerController.Create(3, spawnPoints[2].position, TeamType.TeamTwo, PlayerType.Collector);
        players[3] = PlayerController.Create(4, spawnPoints[3].position, TeamType.TeamTwo, PlayerType.Slasher);

        foreach (PlayerController player in players)
            player.active = false;
    }

    private void AssignAllowedIngredients()
    {
        allowedIngredients = new List<IngredientType>();
        for (int i = 0; i < Configurator.maxAllowedIngredientCount; i++)
        {
            IngredientType inType = EnumUtility.GetRandomEnum<IngredientType>();

            if (inType == IngredientType.None || inType == IngredientType.Remove)
            {
                i--;
                continue;
            }

            bool isUnique = true;
            foreach (IngredientType t in allowedIngredients)
            {
                if (inType == t)
                {
                    isUnique = false;
                    i--;
                }
            }

            if (isUnique)
            {
                allowedIngredients.Add(inType);
            }
        }
    }

    private void LoadCauldrons()
    {
        ingredientSequence = new List<IngredientType>();
        for (int i = 0; i < Configurator.cauldronIngredientSlots; i++)
        {
            IngredientType randomIngredientType = allowedIngredients[Random.Range(0, allowedIngredients.Count)];
            ingredientSequence.Add(randomIngredientType);
        }

        GameEventManager.TriggerCauldronIngredientSequenceLoaded(ingredientSequence);

        foreach (IngredientType ingredientType in ingredientSequence)
        {
            Debug.Log("Ingredient sequence: " + ingredientType);
        }
    }

    private IngredientType GetRandomIngredientTypeForSpawnCritter()
    {
        List<IngredientType> possibleIngredients = new List<IngredientType>();
        foreach (IngredientType t in allowedIngredients)
        {
            possibleIngredients.Add(t);
            possibleIngredients.Add(t);
        }

        possibleIngredients.Add(IngredientType.None);
        possibleIngredients.Add(IngredientType.Remove);

        IngredientType ingredientType = possibleIngredients[Random.Range(0, possibleIngredients.Count)];

        return ingredientType;
    }

    private IEnumerator SpawnCritterCoroutine()
    {
        while (gameState == GameState.Play)
        {
            if (critters.Count < Configurator.maxCritterCount)
            {
                Transform spawnPoint = critterSpawnPoints[Random.Range(0, critterSpawnPoints.Length)];

                IngredientType ingredientType = GetRandomIngredientTypeForSpawnCritter();

                GameObject effect = Instantiate((GameObject)Resources.Load("critterAppearEffect"));
                effect.transform.position = spawnPoint.position + Vector3.forward + Vector3.up * 0.06f;

                Destroy(effect, 0.5f);
                yield return new WaitForSeconds(0.45f);

                CritterController critter = CritterController.Create(spawnPoint.position, ingredientType);
                critters.Add(critter);
            }

            yield return new WaitForSeconds(Configurator.critterSpawnInterval);
        }
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
            Application.Quit();
    }
}
