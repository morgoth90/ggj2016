﻿using System;
using System.Collections.Generic;

public class GameEventManager
{
    public static Action<GameState> OnGameStateChanged;
    public static Action<CritterController> OnCritterDied;
    public static Action<List<IngredientType>> OnCauldronIngredientSequenceLoaded;
    public static Action<TeamType> OnTeamWin;
    public static Action<IngredientType, TeamType, int> OnRightIngredientDropped;
    public static Action<IngredientType, TeamType, int> OnWrongIngredientDropped;
    public static Action<TeamType, int> OnRemoveIngredientDropped;
    public static Action OnCameraShaked;

    public static void TriggerGameStateChanged(GameState gameState)
    {
        if (OnGameStateChanged != null)
        {
            OnGameStateChanged(gameState);
        }
    }

    public static void TriggerCritterDied(CritterController critter)
    {
        if (OnCritterDied != null)
        {
            OnCritterDied(critter);
        }
    }

    public static void TriggerCauldronIngredientSequenceLoaded(List<IngredientType> ingredientSequence)
    {
        if (OnCauldronIngredientSequenceLoaded != null)
        {
            OnCauldronIngredientSequenceLoaded(ingredientSequence);
        }
    }

    public static void TriggerTeamWin(TeamType teamType)
    {
        if (OnTeamWin != null)
        {
            OnTeamWin(teamType);
        }
    }

    public static void TriggerRightIngredientDropped(IngredientType ingredientType, TeamType teamType, int slotIndex)
    {
        if (OnRightIngredientDropped != null)
        {
            OnRightIngredientDropped(ingredientType, teamType, slotIndex);
        }
    }

    public static void TriggerWrongIngredientDropped(IngredientType ingredientType, TeamType teamType, int slotIndex)
    {
        if (OnWrongIngredientDropped != null)
        {
            OnWrongIngredientDropped(ingredientType, teamType, slotIndex);
        }
    }

    public static void TriggerRemoveIngredientDropped(TeamType teamType, int slotIndex)
    {
        if (OnRemoveIngredientDropped != null)
        {
            OnRemoveIngredientDropped(teamType, slotIndex);
        }
    }

    public static void TriggerCameraShake()
    {
        if (OnCameraShaked != null)
        {
            OnCameraShaked();
        }
    }
}
