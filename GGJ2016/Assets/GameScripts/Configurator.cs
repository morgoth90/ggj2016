﻿public class Configurator
{
    public static readonly int maxAllowedIngredientCount = 3;
    public static readonly int maxCritterCount = 10;
    public static readonly float slasherSpeed = 50f;
    public static readonly float collectorSpeed = 40f;
    public static readonly float critterSpeed = 20f;
    public static readonly float critterSpawnInterval = 4f;
    public static readonly float critterMoveTriggerRange = 0.5f;
    public static readonly float critterMoveRandomlyMinInterval = 1f;
    public static readonly float critterMoveRandomlyMaxInterval = 3f;
    public static readonly float critterMoveRandomlyMaxDirection = 2f;
    public static readonly float playerAttackCooldown = 0.7f;
    public static readonly float playerPickUpCooldown = 0.2f;
    public static readonly int cauldronIngredientSlots = 5;
    public static readonly float ingredientDestroyItselfDuration = 25f;
}
