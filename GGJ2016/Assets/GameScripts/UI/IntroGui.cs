﻿using UnityEngine;

public class IntroGui : MonoBehaviour
{
    public RectTransform fader;

    void Start()
    {
        fader.anchoredPosition = new Vector2(-1300, -1300);
    }

    bool pressed = false;
    private bool isButtonPressSFXPlayed = false;

    void Update()
    {
        if (Input.anyKeyDown)
        {
            pressed = true;
        }

        if (pressed)
        {
            if (!isButtonPressSFXPlayed)
            {
                isButtonPressSFXPlayed = true;
                AudioEventManager.TriggerPlaySFX(SFXType.IntroButton);
            }

            fader.anchoredPosition = Vector2.MoveTowards(fader.anchoredPosition, new Vector2(0, 0), Time.deltaTime * 3200f);
            if (fader.anchoredPosition.magnitude == 0f)
                UnityEngine.SceneManagement.SceneManager.LoadScene("main");
        }
    }
}
