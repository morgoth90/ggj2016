﻿using UnityEngine;
using UnityEngine.UI;

public class IngredientSlot : MonoBehaviour
{
    public Sprite Z_Sprite;
    public Sprite Food_Sprite;
    public Sprite Eye_Sprite;
    public Sprite Crystal_Sprite;
    public Sprite Potion_Sprite;
    public Sprite Like_Sprite;
    public Sprite Battery_Sprite;
    public Sprite Rune_Sprite;
    public Sprite Leaf_Sprite;
    public Sprite Spell_Sprite;
    public Sprite Log_Sprite;

    public Image IngredientImage;
    public Image RightImage;
    public Image WrongImage;

    public int slotIndex;

    void Awake()
    {
        IngredientImage.gameObject.SetActive(false);

        RightImage.color = new Color(1f, 1f, 1f, 0f);
        WrongImage.color = new Color(1f, 1f, 1f, 0f);
    }

    void OnEnable()
    {
        GameEventManager.OnRightIngredientDropped += OnRightIngredientDropped;
        GameEventManager.OnWrongIngredientDropped += OnWrongIngredientDropped;
    }

    void OnDisable()
    {
        GameEventManager.OnRightIngredientDropped -= OnRightIngredientDropped;
        GameEventManager.OnWrongIngredientDropped -= OnWrongIngredientDropped;
    }

    private void PopulateIngredientImage(IngredientType ingredientType)
    {
        switch (ingredientType)
        {
            case IngredientType.Z:
                IngredientImage.sprite = Z_Sprite;
                break;

            case IngredientType.Food:
                IngredientImage.sprite = Food_Sprite;
                break;

            case IngredientType.Eye:
                IngredientImage.sprite = Eye_Sprite;
                break;

            case IngredientType.Crystal:
                IngredientImage.sprite = Crystal_Sprite;
                break;

            case IngredientType.Potion:
                IngredientImage.sprite = Potion_Sprite;
                break;

            case IngredientType.Like:
                IngredientImage.sprite = Like_Sprite;
                break;

            case IngredientType.Battery:
                IngredientImage.sprite = Battery_Sprite;
                break;

            case IngredientType.Rune:
                IngredientImage.sprite = Rune_Sprite;
                break;

            case IngredientType.Leaf:
                IngredientImage.sprite = Leaf_Sprite;
                break;

            case IngredientType.Spell:
                IngredientImage.sprite = Spell_Sprite;
                break;

            case IngredientType.Log:
                IngredientImage.sprite = Log_Sprite;
                break;
        }
    }

    private void OnRightIngredientDropped(IngredientType ingredientType, TeamType teamType, int slotIndex)
    {
        if (this.slotIndex == slotIndex)
        {
            HighlightImage(RightImage);

            IngredientImage.gameObject.SetActive(true);

            PopulateIngredientImage(ingredientType);
        }
    }

    private void OnWrongIngredientDropped(IngredientType ingredientType, TeamType teamType, int slotIndex)
    {
        if (this.slotIndex == slotIndex)
        {
            HighlightImage(WrongImage);
        }
    }

    private void HighlightImage(Image image)
    {
        LeanTween.color(image.GetComponent<RectTransform>(), new Color(1f, 1f, 1f, 1f), 0.5f).setOnComplete(() =>
        {
            LeanTween.color(image.GetComponent<RectTransform>(), new Color(1f, 1f, 1f, 0f), 0.5f).setDelay(0.5f);
        });
    }
}
