﻿using UnityEngine;
using System.Collections;
using GamepadInput;

public class RetryGui : MonoBehaviour
{
    public Sprite[] yesSprite, noSprite;

    public UnityEngine.UI.Image yes, no;

    int selected = 0;

    bool recover = true;
    float lastChange = 0;

    public static void Create()
    {
        GameObject go = Instantiate((GameObject)Resources.Load("RetryGui"));
        go.transform.SetParent(GameManager.Instance.guiCanvas, false);
        AudioEventManager.TriggerMusicVolumeDown();
    }

    void Update()
    {
        if (Time.time - lastChange > 0.2f && Mathf.Abs(GamePad.GetAxis(GamePad.Axis.LeftStick, GamePad.Index.Any).x) > 0.2f)
        {
            selected = (selected + 1) % 2;
            lastChange = Time.time;
        }

        if (selected == 0)
        {
            yes.sprite = yesSprite[1];
            yes.transform.localScale = Vector3.one * 1.5f;
            no.sprite = noSprite[0];
            no.transform.localScale = Vector3.one * 1f;
        }
        else
        {
            yes.sprite = yesSprite[0];
            yes.transform.localScale = Vector3.one * 1f;
            no.sprite = noSprite[1];
            no.transform.localScale = Vector3.one * 1.5f;
        }

        if (GamePad.GetButtonDown(GamePad.Button.A, GamePad.Index.Any) ||
            GamePad.GetButtonDown(GamePad.Button.B, GamePad.Index.Any) ||
            GamePad.GetButtonDown(GamePad.Button.X, GamePad.Index.Any) ||
            GamePad.GetButtonDown(GamePad.Button.Y, GamePad.Index.Any))
        {
            if (selected == 0)
                Application.LoadLevel(Application.loadedLevel);
            else
                Application.Quit();
        }
    }
}
