﻿using UnityEngine;
using UnityEngine.UI;

public class TeamProgressPanel : MonoBehaviour
{
    public Sprite EmptySprite;
    public Sprite FilledSprite;

    public Image[] ProgressSlotImages;

    public TeamType teamType;

    void OnEnable()
    {
        GameEventManager.OnRightIngredientDropped += OnRightIngredientDropped;
        GameEventManager.OnWrongIngredientDropped += OnWrongIngredientDropped;
        GameEventManager.OnRemoveIngredientDropped += OnRemoveIngredientDropped;
    }

    void OnDisable()
    {
        GameEventManager.OnRightIngredientDropped -= OnRightIngredientDropped;
        GameEventManager.OnWrongIngredientDropped -= OnWrongIngredientDropped;
        GameEventManager.OnRemoveIngredientDropped -= OnRemoveIngredientDropped;
    }

    private void OnRightIngredientDropped(IngredientType ingredientType, TeamType teamType, int slotIndex)
    {
        if (this.teamType == teamType)
        {
            ProgressSlotImages[slotIndex].sprite = FilledSprite;
        }
    }

    private void OnWrongIngredientDropped(IngredientType ingredientType, TeamType teamType, int slotIndex)
    {
        if (this.teamType == teamType)
        {

        }
    }

    private void OnRemoveIngredientDropped(TeamType teamType, int slotIndex)
    {
        if (this.teamType == teamType)
        {
            ProgressSlotImages[slotIndex].sprite = EmptySprite;
        }
    }
}
