﻿using UnityEngine;
using System.Collections;

public class CountdownGui : MonoBehaviour
{
    public RectTransform fader;
    public Animator animator;

    public UnityEngine.UI.Image bg;

    void Start()
    {
        startTime = Time.time;
        fader.anchoredPosition = Vector2.zero;
    }

    float startTime = 0;
    private readonly float firstBlipSFXPlayTime = 1.75f;
    private readonly float secondBlipSFXPlayTime = 3f;
    private readonly float thirdBlipSFXPlayTime = 4.25f;
    private readonly float countdownEndSFXPlayTime = 5.5f;
    private bool isFirstBlipSFXPlayed = false;
    private bool isSecondBlipSFXPlayed = false;
    private bool isThirdBlipSFXPlayed = false;
    private bool isCountdownEndSFXPlayed = false;

    void Update()
    {
        if (Time.time - startTime < 0.5f)
        {
            return;
        }

        if (Time.time - startTime > 1.5f && animator.enabled == false)
        {
            animator.enabled = true;
        }

        if (Time.time - startTime > firstBlipSFXPlayTime && !isFirstBlipSFXPlayed)
        {
            AudioEventManager.TriggerPlaySFX(SFXType.CountdownBlip);
            isFirstBlipSFXPlayed = true;
        }

        if (Time.time - startTime > secondBlipSFXPlayTime && !isSecondBlipSFXPlayed)
        {
            AudioEventManager.TriggerPlaySFX(SFXType.CountdownBlip);
            isSecondBlipSFXPlayed = true;
        }

        if (Time.time - startTime > thirdBlipSFXPlayTime && !isThirdBlipSFXPlayed)
        {
            AudioEventManager.TriggerPlaySFX(SFXType.CountdownBlip);
            isThirdBlipSFXPlayed = true;
        }

        if (Time.time - startTime > countdownEndSFXPlayTime && !isCountdownEndSFXPlayed)
        {
            AudioEventManager.TriggerPlaySFX(SFXType.CountdownEnd);
            isCountdownEndSFXPlayed = true;

            AudioEventManager.TriggerMusicStart();
        }

        Vector2 dest = new Vector2(1300, 1300);
        fader.anchoredPosition = Vector2.MoveTowards(fader.anchoredPosition, dest, Time.deltaTime * 3900f);

        if (Time.time - startTime > 7.5f)
        {
            Destroy(gameObject);
            GameManager.Instance.StartGame();
        }


    }
}
