﻿using UnityEngine;

public class IngredientController : MonoBehaviour
{
    public static IngredientController Create(IngredientType ingredientType, CritterController critter)
    {
        GameObject go = Instantiate((GameObject)Resources.Load("Ingredient"));
        IngredientController ic = go.GetComponent<IngredientController>();

        ic.Initialize(ingredientType, critter);

        return ic;
    }

    public Sprite[] ingredientTypeSprites;
    public GameObject shadow;

    private Animator animator;
    private new Collider2D collider;
    private new Rigidbody2D rigidbody;
    private SpriteRenderer spriteRenderer;
    private IngredientType type;
    private IngredientState state;
    private CritterController critter;

    private float dropTime = -10f;
    private Vector2 dropDirection;
    private float destroyCountdown;

    void Awake()
    {
        animator = GetComponent<Animator>();
        collider = GetComponent<Collider2D>();
        rigidbody = GetComponent<Rigidbody2D>();
        spriteRenderer = GetComponent<SpriteRenderer>();

        collider.enabled = false;
    }

    void Update()
    {
        HandleDestroyUpdate();

        float t = 0.5f;
        if (Time.time - dropTime < t)
        {
            float dropI = (Time.time - dropTime) / t;
            float yOffset = 0;
            float yOffsetShadow = 0;

            if (dropI > 0.3f)
            {
                yOffset = ((Mathf.Abs(-Mathf.Sin((dropI - 0.3f) * 5.5f))) / 2 - 0.22f) * 110f * (1f - (dropI - 0.3f));
                yOffsetShadow = ((Mathf.Abs(-Mathf.Cos((dropI - 0.3f) * 5.5f))) / 2 - 0.22f) * 110f * (1f - (dropI - 0.3f));
            }

            rigidbody.velocity = (dropDirection * 150f * Time.deltaTime * (1f - dropI)) + Vector2.up * yOffset * Time.deltaTime;

            shadow.transform.localPosition = new Vector3(0, -0.55f - yOffsetShadow * 0.006f, 1f);
        }
        else
        {
            shadow.transform.localPosition = new Vector3(0, -0.55f, 1f);
            if (gameObject.layer != LayerMask.NameToLayer("Ingredient"))
            {
                gameObject.layer = LayerMask.NameToLayer("Ingredient");
                Vector3 p = transform.position;
                p.z = -0.2f;
                transform.position = p;
            }
        }
    }

    public void Drop(Vector2 direction = default(Vector2))
    {
        shadow.SetActive(true);
        state = IngredientState.OnFloor;
        destroyCountdown = 0f;

        Vector3 p = transform.position;
        p.z = -1;
        transform.position = p;

        gameObject.layer = LayerMask.NameToLayer("DroppedIngredient");

        dropTime = Time.time;
        dropDirection = direction;

        collider.enabled = true;
        rigidbody.isKinematic = false;

        transform.SetParent(null);

        AudioEventManager.TriggerPlaySFX(SFXType.IngredientDrop);
    }

    public void PickUp(PlayerController player)
    {
        shadow.SetActive(false);
        state = IngredientState.OnPlayer;

        dropTime = 0;
        dropDirection = Vector2.zero;

        collider.enabled = false;
        rigidbody.isKinematic = true;

        transform.SetParent(player.transform);
        transform.localPosition = new Vector3(0f, 1f, -1f);
    }

    private void Initialize(IngredientType type, CritterController critter)
    {
        shadow.SetActive(false);
        rigidbody.isKinematic = true;

        state = IngredientState.OnCritter;

        collider.enabled = false;

        transform.SetParent(critter.transform);
        transform.localPosition = new Vector3(0f, 1f, -1f);

        this.type = type;
        this.critter = critter;

        if (type == IngredientType.Remove)
        {
            animator.enabled = true;
        }
        else
        {
            spriteRenderer.sprite = ingredientTypeSprites[(int)type - 2];
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (gameObject.layer != LayerMask.NameToLayer("DroppedIngredient"))
        {
            return;
        }

        if (other.gameObject.layer == LayerMask.NameToLayer("Cauldron"))
        {
            Cauldron cauldron = other.GetComponent<Cauldron>();

            if (cauldron.DropIngredient(type))
            {
                Destroy(gameObject);
            }
        }
    }

    private void HandleDestroyUpdate()
    {
        if (state == IngredientState.OnFloor)
        {
            destroyCountdown += Time.deltaTime;
        }

        if (destroyCountdown > Configurator.ingredientDestroyItselfDuration)
        {
            ParticleFactory.CreateParticle(ParticleType.IngredientDisappear, transform.position);

            Destroy(gameObject);
        }
    }

    public IngredientType Type
    {
        get
        {
            return type;
        }
    }

    public IngredientState State
    {
        get
        {
            return state;
        }
    }
}
