﻿using UnityEngine;

public class ParticleFactory
{
    public static GameObject CreateParticle(ParticleType type, Vector3 position)
    {
        GameObject go = null;

        switch (type)
        {
            case ParticleType.Hit:
                go = GameObject.Instantiate((GameObject)Resources.Load("Particles/Hit"));
                break;

            case ParticleType.BloodSplash:
                go = GameObject.Instantiate((GameObject)Resources.Load("Particles/BloodSplash"));
                break;

            case ParticleType.GoodSmoke:
                go = GameObject.Instantiate((GameObject)Resources.Load("Particles/GoodSmoke"));
                break;

            case ParticleType.BadSmoke:
                go = GameObject.Instantiate((GameObject)Resources.Load("Particles/BadSmoke"));
                break;

            case ParticleType.RemoveIngredientDrop:
                go = GameObject.Instantiate((GameObject)Resources.Load("Particles/RemoveIngredientDrop"));
                break;

            case ParticleType.IngredientDisappear:
                go = GameObject.Instantiate((GameObject)Resources.Load("Particles/IngredientDisappear"));
                break;

            case ParticleType.CritterDeath:
                go = GameObject.Instantiate((GameObject)Resources.Load("Particles/CritterDeath"));
                break;
        }

        go.transform.position = position;

        return go;
    }
}
