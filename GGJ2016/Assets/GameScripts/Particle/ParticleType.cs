﻿public enum ParticleType
{
    Hit,
    BloodSplash,
    GoodSmoke,
    BadSmoke,
    RemoveIngredientDrop,
    IngredientDisappear,
    CritterDeath,
}
