﻿using UnityEngine;

public class Particle : MonoBehaviour
{
    public float destroyTime;

    void Start()
    {
        LeanTween.delayedCall(destroyTime, () =>
        {
            Destroy(gameObject);
        });
    }
}
