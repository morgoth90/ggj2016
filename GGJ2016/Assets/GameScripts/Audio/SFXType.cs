﻿public enum SFXType
{
    IntroButton,
    ButtonConfirm,
    CountdownBlip,
    CountdownEnd,
    Slash,
    Hit,
    IngredientPickUp,
    IngredientDrop,
    RightIngredientPut,
    WrongIngredientPut,
    RemoveIngredientPut,
    CritterAppear,
}
