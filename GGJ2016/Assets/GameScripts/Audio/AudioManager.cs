﻿using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public List<AudioClip> MusicAudioClips;
    public List<AudioClip> IntroButtonAudioClips;
    public List<AudioClip> ButtonConfirmAudioClips;
    public List<AudioClip> CountdownBlipAudioClips;
    public List<AudioClip> CountdownEndAudioClips;
    public List<AudioClip> WalkAudioClips;
    public List<AudioClip> SlashAudioClips;
    public List<AudioClip> HitAudioClips;
    public List<AudioClip> IngredientDropAudioClips;
    public List<AudioClip> RightIngredientPutAudioClips;
    public List<AudioClip> WrongIngredientPutAudioClips;
    public List<AudioClip> RemoveIngredientPutAudioClips;
    public List<AudioClip> CritterAppearAudioClips;

    private AudioSource audioSource;
    private AudioReverbFilter audioReverbFilter;
    private float walkSFXCooldown;

    void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        audioReverbFilter = GetComponent<AudioReverbFilter>();

        audioReverbFilter.enabled = false;
    }

    void OnEnable()
    {
        AudioEventManager.OnSFXPlayed += OnSFXPlayed;
        AudioEventManager.OnMoveSFXPlayed += OnMoveSFXPlayed;
        AudioEventManager.OnMusicStarted += OnMusicStarted;
        AudioEventManager.OnMusicVolumeDown += OnMusicVolumeDown;
    }

    void OnDisable()
    {
        AudioEventManager.OnSFXPlayed -= OnSFXPlayed;
        AudioEventManager.OnMoveSFXPlayed -= OnMoveSFXPlayed;
        AudioEventManager.OnMusicStarted -= OnMusicStarted;
        AudioEventManager.OnMusicVolumeDown -= OnMusicVolumeDown;
    }

    private void OnSFXPlayed(SFXType type)
    {
        List<AudioClip> possibleSFXs = new List<AudioClip>();

        switch (type)
        {
            case SFXType.IntroButton:
                possibleSFXs = IntroButtonAudioClips;
                break;

            case SFXType.ButtonConfirm:
                possibleSFXs = ButtonConfirmAudioClips;
                break;

            case SFXType.CountdownBlip:
                possibleSFXs = CountdownBlipAudioClips;
                break;

            case SFXType.CountdownEnd:
                possibleSFXs = CountdownEndAudioClips;
                break;

            case SFXType.Slash:
                possibleSFXs = SlashAudioClips;
                break;

            case SFXType.Hit:
                possibleSFXs = HitAudioClips;
                break;

            case SFXType.IngredientDrop:
                possibleSFXs = IngredientDropAudioClips;
                break;

            case SFXType.RightIngredientPut:
                possibleSFXs = RightIngredientPutAudioClips;
                break;

            case SFXType.WrongIngredientPut:
                possibleSFXs = WrongIngredientPutAudioClips;
                break;

            case SFXType.RemoveIngredientPut:
                possibleSFXs = RemoveIngredientPutAudioClips;
                break;

            case SFXType.CritterAppear:
                possibleSFXs = CritterAppearAudioClips;
                break;
        }

        AudioClip clip = possibleSFXs[Random.Range(0, possibleSFXs.Count)];

        audioSource.PlayOneShot(clip);
    }

    private void OnMoveSFXPlayed()
    {
        walkSFXCooldown += Time.deltaTime;
        if (walkSFXCooldown > 0.25f)
        {
            AudioClip clip = WalkAudioClips[Random.Range(0, WalkAudioClips.Count)];

            audioSource.PlayOneShot(clip);

            walkSFXCooldown = 0f;
        }
    }

    private void OnMusicStarted()
    {
        audioSource.clip = MusicAudioClips[Random.Range(0, MusicAudioClips.Count)];
        audioSource.Play(1);
    }

    private void OnMusicVolumeDown()
    {
        audioReverbFilter.enabled = true;
        audioSource.volume = 0.4f;
    }
}
