﻿using System;

public class AudioEventManager
{
    public static Action<SFXType> OnSFXPlayed;
    public static Action OnMoveSFXPlayed;
    public static Action OnMusicStarted;
    public static Action OnMusicVolumeDown;

    public static void TriggerPlaySFX(SFXType type)
    {
        if (OnSFXPlayed != null)
        {
            OnSFXPlayed(type);
        }
    }

    public static void TriggerMoveSFXPlayed()
    {
        if (OnMoveSFXPlayed != null)
        {
            OnMoveSFXPlayed();
        }
    }

    public static void TriggerMusicStart()
    {
        if (OnMusicStarted != null)
        {
            OnMusicStarted();
        }
    }

    public static void TriggerMusicVolumeDown()
    {
        if (OnMusicVolumeDown != null)
        {
            OnMusicVolumeDown();
        }
    }
}
