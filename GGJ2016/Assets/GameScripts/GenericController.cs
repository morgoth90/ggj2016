﻿using UnityEngine;
using System.Collections;

public class GenericController : MonoBehaviour
{
    public float speed = 50f;

    new Rigidbody2D rigidbody;
    protected Animator animator;

    protected Vector2 direction;

    protected bool stunned
    {
        get
        {
            return Time.time - stunTime < 1f;
        }
    }

    protected virtual void Awake()
    {
        animator = GetComponent<Animator>();
        rigidbody = GetComponent<Rigidbody2D>();
    }

    public void Move(Vector2 direction)
    {
        if (stunned)
            return;

        this.direction = direction.normalized;

        rigidbody.velocity = (direction * Time.deltaTime * speed);

        Vector2 vel = rigidbody.velocity;

        animator.SetFloat("movX", vel.x);
        animator.SetFloat("movY", vel.y);
    }

    bool lastFrameStunned = false;
    protected virtual void Update()
    {
        animator.SetFloat("speed", rigidbody.velocity.magnitude);

        if (stunned == false && lastFrameStunned == true)
            animator.SetTrigger("recovery");

        lastFrameStunned = stunned;
    }

    float stunTime = -10;
    public virtual void Hit(GenericController sender)
    {
        GameEventManager.TriggerCameraShake();

        ParticleFactory.CreateParticle(ParticleType.Hit, transform.position);

        AudioEventManager.TriggerPlaySFX(SFXType.Hit);

        animator.SetTrigger("stun");
        stunTime = Time.time;
        float mul = 1;
        if (sender is CritterController)
            mul = 0.5f;
        rigidbody.velocity = (transform.position - sender.transform.position).normalized * 400 * Time.deltaTime * mul;

    }

	
}
