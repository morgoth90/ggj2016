﻿using System;

public class EnumUtility
{
    public static T GetRandomEnum<T>()
    {
        Array A = Enum.GetValues(typeof(T));
        T V = (T)A.GetValue(UnityEngine.Random.Range(0, A.Length));
        return V;
    }

    public static T GetEnumByString<T>(string str)
    {
        T t = (T)Enum.Parse(typeof(T), str);
        return t;
    }
}
