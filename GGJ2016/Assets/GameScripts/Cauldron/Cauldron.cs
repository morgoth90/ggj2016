﻿using System.Collections.Generic;
using UnityEngine;

public class Cauldron : MonoBehaviour
{
    public Animator monsterAnimator;
    public TeamType teamType;

    private List<IngredientType> ingredientSequence;
    private int ingredientSequenceIndex;

    private Animator animator;

    void Awake()
    {
        animator = GetComponent<Animator>();
        ingredientSequence = new List<IngredientType>();
    }

    void OnEnable()
    {
        GameEventManager.OnCauldronIngredientSequenceLoaded += OnCauldronIngredientSequenceLoaded;
    }

    void OnDisable()
    {
        GameEventManager.OnCauldronIngredientSequenceLoaded -= OnCauldronIngredientSequenceLoaded;
    }

    void Start()
    {
        animator.SetBool("active", true);
    }

    public bool DropIngredient(IngredientType ingredientType)
    {
        if (animator.GetBool("active") == false)
        {
            return false;
        }

        if (ingredientSequence.Count <= ingredientSequenceIndex)
        {
            return false;
        }

        if (ingredientSequence[ingredientSequenceIndex] == ingredientType)
        {
            PutRightIngredient(ingredientType);
        }
        else
        {
            if (ingredientType == IngredientType.Remove)
            {
                PutRemoveIngredient();
            }
            else
            {
                PutWrongIngredient(ingredientType);
            }
        }
        return true;
    }

    private void PutRightIngredient(IngredientType ingredientType)
    {
        ParticleFactory.CreateParticle(ParticleType.GoodSmoke, transform.position + new Vector3(0f, 0.05f, 0f));

        AudioEventManager.TriggerPlaySFX(SFXType.RightIngredientPut);

        GameEventManager.TriggerRightIngredientDropped(ingredientType, teamType, ingredientSequenceIndex);

        ingredientSequenceIndex++;

        if (ingredientSequenceIndex == ingredientSequence.Count)
        {
            GameEventManager.TriggerTeamWin(teamType);
        }
    }

    GameObject locked = null;

    float disableTime = -10;
    private void PutWrongIngredient(IngredientType ingredientType)
    {
        ParticleFactory.CreateParticle(ParticleType.BadSmoke, transform.position + new Vector3(0f, 0.05f, 0f));

        AudioEventManager.TriggerPlaySFX(SFXType.WrongIngredientPut);

        GameEventManager.TriggerWrongIngredientDropped(ingredientType, teamType, ingredientSequenceIndex);
        //disable cauldron for x seconds
        disableTime = Time.time;

        locked = Instantiate((GameObject)Resources.Load("locked"));
        locked.transform.SetParent(transform, false);
        locked.transform.localPosition = new Vector3(0, 0, -10);
    }

    private void PutRemoveIngredient()
    {
        ingredientSequenceIndex--;

        if (ingredientSequenceIndex < 0)
        {
            ingredientSequenceIndex = 0;
        }

        GameEventManager.TriggerCameraShake();

        ParticleFactory.CreateParticle(ParticleType.RemoveIngredientDrop, transform.position + new Vector3(0f, 0.05f, 0f));

        AudioEventManager.TriggerPlaySFX(SFXType.RemoveIngredientPut);

        GameEventManager.TriggerRemoveIngredientDropped(teamType, ingredientSequenceIndex);
    }

    void Update()
    {
        bool isLocked = Time.time - disableTime > 10f;

        if (isLocked && locked != null)
        {
            Destroy(locked);
            locked = null;
        }
        animator.SetBool("active", isLocked);
        monsterAnimator.SetFloat("size", (float)ingredientSequenceIndex);
    }

    private void OnCauldronIngredientSequenceLoaded(List<IngredientType> ingredientSequence)
    {
        this.ingredientSequence = ingredientSequence;

        ingredientSequenceIndex = 0;
    }
}
