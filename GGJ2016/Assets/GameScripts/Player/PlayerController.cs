﻿using UnityEngine;
using GamepadInput;
using System.Collections;

public class PlayerController : GenericController
{
    public static PlayerController Create(int playerNumber, Vector3 position, TeamType teamType, PlayerType type)
    {
        GameObject go = Instantiate((GameObject)Resources.Load("Player"));
        PlayerController pc = go.GetComponent<PlayerController>();

        switch (type)
        {
            case PlayerType.Slasher:
                {
                    switch (teamType)
                    {
                        case TeamType.TeamOne:
                            pc.animator.runtimeAnimatorController = pc.slasherAnimators[0];
                            break;

                        case TeamType.TeamTwo:
                            pc.animator.runtimeAnimatorController = pc.slasherAnimators[1];
                            break;
                    }

                    pc.speed = Configurator.slasherSpeed;
                }
                break;

            case PlayerType.Collector:
                {
                    switch (teamType)
                    {
                        case TeamType.TeamOne:
                            pc.animator.runtimeAnimatorController = pc.collectorAnimators[0];
                            break;

                        case TeamType.TeamTwo:
                            pc.animator.runtimeAnimatorController = pc.collectorAnimators[1];
                            break;
                    }

                    pc.speed = Configurator.collectorSpeed;
                }
                break;
        }
        position.z = -0.2f;
        go.transform.position = position;
        pc.playerType = type;
        pc.teamType = teamType;

        pc.transform.FindChild(teamType.ToString()).gameObject.SetActive(true);

        pc.playerNumber = playerNumber;

        return pc;
    }

    public RuntimeAnimatorController[] collectorAnimators;
    public RuntimeAnimatorController[] slasherAnimators;

    bool playerNumberChanged = false;
    int _playerNumber;
    public int playerNumber
    {
        get
        {
            return _playerNumber;
        }
        set
        {
            _playerNumber = value;
            playerNumberChanged = true;
        }
    }
    public PlayerType playerType;
    public TeamType teamType;
    private float lastAttackTime = 0f;
    private float lastPickUpTime = 0f;
    private IngredientController carryingIngredient;

    protected void Start()
    {
        Move(new Vector2(0, -0.2f));
    }

    public bool active = true;
    protected override void Update()
    {
        base.Update();

        if (!active)
            return;

        Vector2 input = GamePad.GetAxis(GamePad.Axis.LeftStick, (GamePad.Index)playerNumber);

        if (input.magnitude > 0.2f && CanMove())
        {
            Vector2 velocity = GetComponent<Rigidbody2D>().velocity;

            if (!stunned && velocity.magnitude > 0.5f)
            {
                AudioEventManager.TriggerMoveSFXPlayed();
            }

            Move(input);
        }

        if (GamePad.GetButtonDown(GamePad.Button.A, (GamePad.Index)playerNumber) ||
            GamePad.GetButtonDown(GamePad.Button.B, (GamePad.Index)playerNumber) ||
            GamePad.GetButtonDown(GamePad.Button.X, (GamePad.Index)playerNumber) ||
            GamePad.GetButtonDown(GamePad.Button.Y, (GamePad.Index)playerNumber))
        {
            HandleButtonPress();
        }

        if (!playerNumberChanged && GamePad.GetButtonDown(GamePad.Button.RightShoulder, (GamePad.Index)playerNumber))
            GameManager.Instance.SwapTeamControls(teamType);

        playerNumberChanged = false;
    }

    private void HandleButtonPress()
    {
        switch (playerType)
        {
            case PlayerType.Slasher:
                {
                    if (!CanAttack())
                    {
                        return;
                    }

                    Attack();
                }
                break;

            case PlayerType.Collector:
                {
                    if (carryingIngredient == null)
                    {
                        float radius = GetComponent<CircleCollider2D>().radius * transform.localScale.x;
                        foreach (Collider2D hit in Physics2D.OverlapCircleAll(transform.position, radius, 1 << LayerMask.NameToLayer("Ingredient")))
                        {
                            if (hit != null)
                            {
                                lastPickUpTime = Time.time;

                                animator.SetBool("carry", true);

                                IngredientController ingredient = hit.GetComponent<IngredientController>();

                                ingredient.PickUp(this);

                                carryingIngredient = ingredient;

                                AudioEventManager.TriggerPlaySFX(SFXType.IngredientDrop);

                                break;
                            }
                        }
                    }
                    else
                    {
                        Drop(direction);
                    }
                }
                break;
        }
    }

    void Drop(Vector2 direction)
    {
        if (carryingIngredient == null)
            return;

        animator.SetBool("carry", false);

        carryingIngredient.Drop(direction);

        carryingIngredient = null;
    }

    public void OnAttackEvent()//called from animation
    {
        float radius = GetComponent<CircleCollider2D>().radius * transform.localScale.x;
        foreach (Collider2D hit in Physics2D.OverlapCircleAll(transform.position + (Vector3)direction * radius * 1.2f, radius, 1 << LayerMask.NameToLayer("Player")))
        {
            if (hit != null)
            {
                GenericController controller = hit.GetComponent<GenericController>();

                if (controller != this)
                {
                    controller.Hit(this);
                }
            }
        }

        foreach (Collider2D hit in Physics2D.OverlapCircleAll(transform.position + (Vector3)direction * radius * 1.2f, radius, 1 << LayerMask.NameToLayer("Critter")))
        {
            if (hit != null)
            {
                GenericController controller = hit.GetComponent<GenericController>();

                if (controller != this)
                {
                    controller.Hit(this);
                }
            }
        }
    }


    private bool CanMove()
    {
        switch (playerType)
        {
            case PlayerType.Slasher:
                return Time.time - lastAttackTime > Configurator.playerAttackCooldown;

            case PlayerType.Collector:
                return Time.time - lastPickUpTime > Configurator.playerPickUpCooldown;

            default:
                return false;
        }
    }

    private bool CanAttack()
    {
        switch (playerType)
        {
            case PlayerType.Slasher:
                return Time.time - lastAttackTime > Configurator.playerAttackCooldown;

            case PlayerType.Collector:
                return false;

            default:
                return false;
        }
    }

    private bool CanPickUpIngredient()
    {
        switch (playerType)
        {
            case PlayerType.Slasher:
                return false;

            case PlayerType.Collector:
                return Time.time - lastPickUpTime > Configurator.playerPickUpCooldown;

            default:
                return false;
        }
    }


    void OnCollisionEnter2D(Collision2D coll)
    {
        if (/*playerType == PlayerType.Slasher ||*/ stunned)
            return;

        CritterController cc = coll.gameObject.GetComponent<CritterController>();
        if (cc != null)
        {
            Hit(cc);
        }
    }


    public override void Hit(GenericController sender)
    {
        base.Hit(sender);

        Drop((transform.position - sender.transform.position).normalized * 0.25f);
    }

    private void Attack()
    {
        animator.SetTrigger("attack");
        lastAttackTime = Time.time;

        AudioEventManager.TriggerPlaySFX(SFXType.Slash);
    }
}
