﻿using System.Collections;
using UnityEngine;

public class CritterController : GenericController
{
    public static CritterController Create(Vector3 position, IngredientType ingredientType)
    {
        GameObject go = Instantiate((GameObject)Resources.Load("Critter"));
        CritterController cc = go.GetComponent<CritterController>();

		position.z = -0.2f;
        go.transform.position = position;
        cc.Initialize(ingredientType);

        return cc;
    }

    private IngredientType ingredientType;
    private IngredientController ingredient;
    private bool isWalkingTowardsPlayer = false;

    void Start()
    {
        AudioEventManager.TriggerPlaySFX(SFXType.CritterAppear);

        speed = Configurator.critterSpeed;

        StartCoroutine(MoveRandomlyCoroutine());
    }

    protected override void Update()
    {
        base.Update();

        HandleChasePlayer();
    }

    public override void Hit(GenericController sender)
    {
        base.Hit(sender);
        Die();
    }

    public void Die()
    {
        if (ingredient != null)
        {
            ingredient.Drop(Vector3.down * 0.15f);
        }

        ParticleFactory.CreateParticle(ParticleType.BloodSplash, transform.position);

        GameEventManager.TriggerCritterDied(this);
    }

    private void Initialize(IngredientType ingredientType)
    {
        this.ingredientType = ingredientType;

        if (ingredientType != IngredientType.None)
        {
            ingredient = IngredientController.Create(ingredientType, this);
        }
    }

    private void HandleChasePlayer()
    {
        float minDistance = float.MaxValue;
        int playerIndex = 0;

        for (int i = 0; i < GameManager.Instance.players.Length; i++)
        {
            PlayerController player = GameManager.Instance.players[i];

            float distance = Vector3.Distance(player.transform.position, transform.position);
            if (distance < minDistance)
            {
                minDistance = distance;
                playerIndex = i;
            }
        }

        if (minDistance < Configurator.critterMoveTriggerRange)
        {
            Move(GameManager.Instance.players[playerIndex].transform.position - transform.position);
            isWalkingTowardsPlayer = true;
        }
        else
        {
            isWalkingTowardsPlayer = false;
        }
    }

    private IEnumerator MoveRandomlyCoroutine()
    {
        while (true)
        {
            float moveRandomlyInterval = Random.Range(Configurator.critterMoveRandomlyMinInterval, Configurator.critterMoveRandomlyMaxInterval);
            yield return new WaitForSeconds(moveRandomlyInterval);

            if (!isWalkingTowardsPlayer)
            {
                Vector2 direction = new Vector2(
                    Random.Range(-Configurator.critterMoveRandomlyMaxDirection, Configurator.critterMoveRandomlyMaxDirection),
                    Random.Range(-Configurator.critterMoveRandomlyMaxDirection, Configurator.critterMoveRandomlyMaxDirection));
                Move(direction);
            }
        }
    }
}
