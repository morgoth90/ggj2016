﻿using UnityEngine;

public class CameraShaker : MonoBehaviour
{
    private Vector3 originPosition;
    private Quaternion originRotation;
    private float shakeDecay;
    private float shakeIntensity;
    private Vector3 initialPosition;
    private Quaternion initialRotation;

    void OnEnable()
    {
        GameEventManager.OnCameraShaked += Shake;
    }

    void OnDisable()
    {
        GameEventManager.OnCameraShaked -= Shake;
    }

    void Start()
    {
        initialPosition = transform.position;
        initialRotation = transform.rotation;
    }

    void Update()
    {
        if (shakeIntensity > 0)
        {
            transform.position = originPosition + Random.insideUnitSphere * shakeIntensity;
            transform.rotation = new Quaternion(
            originRotation.x + Random.Range(-shakeIntensity, shakeIntensity) * 0.2f,
            originRotation.y + Random.Range(-shakeIntensity, shakeIntensity) * 0.2f,
            originRotation.z + Random.Range(-shakeIntensity, shakeIntensity) * 0.2f,
            originRotation.w + Random.Range(-shakeIntensity, shakeIntensity) * 0.2f);
            shakeIntensity -= shakeDecay;
        }
        else
        {
            transform.position = initialPosition;
            transform.rotation = initialRotation;
        }
    }

    void Shake()
    {
        originPosition = transform.position;
        originRotation = transform.rotation;
        shakeIntensity = 0.015f;
        shakeDecay = 0.001f;
    }
}